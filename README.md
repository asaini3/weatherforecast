# Weather Forecast

Weather Forecast Android application demo.

1. App has two tabs for weather data, one search icon & options menu in home screen. 
2. In home screen it will show Bangalore weather by default in Today Tab. 
3. We can swipe in both Tabs (TODAY & 5 DAY FORECAST). 
4. We can see temperature in both Celsius or in Fahrenheit through toggle button.
5. In second Tab we can see 5 days forecast weather with 3 hours interval for 5 days.
6. After clicking the search icon, we need to put any city, we can get current and forecast weather of any city.
5. Options menu in home screen, here we have options for clear the search history.