package com.example.wetaherforeacstexercise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wetaherforeacstexercise.R;
import com.example.wetaherforeacstexercise.utils.Constants;
import com.example.wetaherforeacstexercise.utils.TDConverter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchForecastAdapter extends RecyclerView.Adapter<SearchForecastAdapter.ForecastViewHolder>{

    List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherList;
    private Context context;
    private String tempUnit;

    public SearchForecastAdapter(List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherList, Context context, String tempUnit) {
        this.weatherList = weatherList;
        this.context = context;
        this.tempUnit = tempUnit;
    }



    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_forecast_row, parent, false);

        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        // set icon
        String icon = weatherList.get(position).getWeather().get(0).getIcon();
        String iconUrl = Constants.WEATHER_MAP_IMAGE_BASE_URL+icon+".png";
        Picasso.get().load(iconUrl).into(holder.weatherIcon);
        // day name
        String dateNameString = TDConverter.getDay(weatherList.get(position).getDt());
        holder.dateName.setText(dateNameString);
        // day name
        String time = TDConverter.getTime(weatherList.get(position).getDt());
        holder.hour.setText(time);
        // date
        String dateString = TDConverter.getDate(weatherList.get(position).getDt());
        holder.date.setText(dateString);
        // weather in degree celsius/f
        int temp = weatherList.get(position).getMain().getTemp().intValue();
        if (tempUnit.equals("metric")){
            String tempC = temp+" "+ context.getString(R.string.unit_c);
            holder.temp.setText(tempC);
        } else if (tempUnit.equals("imperial")){
            String tempF = temp+" "+ context.getString(R.string.unit_f);
            holder.temp.setText(tempF);
        }
        // weather description
        holder.tempDesc.setText(weatherList.get(position).getWeather().get(0).getDescription());
        //MAX TEMP
        String maxTemp = " " + context.getString(R.string.max_temp).toLowerCase() + ": " +weatherList.get(position).getMain().getTempMax().intValue();
        holder.maxTemp.setText(maxTemp);
        //MIN TEMP
        String minTemp = " " + context.getString(R.string.min_temp).toLowerCase() + ": " +weatherList.get(position).getMain().getTempMin().intValue();
        holder.minTemp.setText(minTemp);
        }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder{
        TextView dateName, date, temp, tempDesc
                , maxTemp, minTemp, hour;
        ImageView weatherIcon;

        public ForecastViewHolder(@NonNull View itemView) {
            super(itemView);
            dateName = itemView.findViewById(R.id.dateNameFTV);
            hour = itemView.findViewById(R.id.hourFTV);
            date = itemView.findViewById(R.id.dateFTV);
            temp = itemView.findViewById(R.id.tempFTV);
            tempDesc = itemView.findViewById(R.id.temp_descriptionFTV);
            maxTemp = itemView.findViewById(R.id.temp_maxFTV);
            minTemp = itemView.findViewById(R.id.temp_minFTV);
            weatherIcon = itemView.findViewById(R.id.weatherIconFTV);
        }
    }

    public void updateCollectionData(List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherList){
        this.weatherList = weatherList;
        notifyDataSetChanged();
    }
}
