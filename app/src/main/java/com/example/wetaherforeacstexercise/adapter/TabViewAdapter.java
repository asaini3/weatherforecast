package com.example.wetaherforeacstexercise.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.wetaherforeacstexercise.utils.Constants;
import com.example.wetaherforeacstexercise.view.fragments.ForecastTabFragment;
import com.example.wetaherforeacstexercise.view.fragments.TodayTabFragment;

public class TabViewAdapter extends FragmentPagerAdapter {
    private Context context;
    public TabViewAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", Constants.DEFAULT_LAT);
        bundle.putDouble("lng", Constants.DEFAULT_LNG);

        switch (i){
            case 0:
                TodayTabFragment todayTabFragment = new TodayTabFragment();
                todayTabFragment.setArguments(bundle);
                return todayTabFragment;

            case 1:
                ForecastTabFragment forecastTabFragment = new ForecastTabFragment();
                forecastTabFragment.setArguments(bundle);
                return forecastTabFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}