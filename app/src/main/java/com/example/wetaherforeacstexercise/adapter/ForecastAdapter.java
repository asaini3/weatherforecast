package com.example.wetaherforeacstexercise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.wetaherforeacstexercise.R;
import com.example.wetaherforeacstexercise.utils.Constants;
import com.example.wetaherforeacstexercise.utils.TDConverter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>{

    List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherMapList;
    private Context context;

    public ForecastAdapter(List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherList, Context context) {
        this.weatherMapList = weatherList;
        this.context = context;
    }



    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forecast_row, parent, false);

        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        // set icon
        String icon = weatherMapList.get(position).getWeather().get(0).getIcon();
        String iconUrl = Constants.WEATHER_MAP_IMAGE_BASE_URL+icon+".png";
        Picasso.get().load(iconUrl).into(holder.weatherIcon);
        // day name
        String dateNameString = TDConverter.getDay(weatherMapList.get(position).getDt());
        holder.dateName.setText(dateNameString);
        // day name
        String time = TDConverter.getTime(weatherMapList.get(position).getDt());
        holder.hourTV.setText(time);
        // date
        String dateString = TDConverter.getDate(weatherMapList.get(position).getDt());
        holder.date.setText(dateString);

        // weather in degree celsius
        int tempInC = weatherMapList.get(position).getMain().getTemp().intValue();
        holder.temp.setText(tempInC+" \u2103");
        // weather description
        holder.tempDesc.setText(weatherMapList.get(position).getWeather().get(0).getDescription());
        // humidity
        String humidity = " " + context.getString(R.string.humidity) +": "+ weatherMapList.get(position).getMain().getHumidity() + " %";
        holder.humidity.setText(humidity);
        // cloud
        String cloud = " " + context.getString(R.string.clouds) +": "+ weatherMapList.get(position).getClouds().getAll() + " %";
        holder.cloud.setText(cloud);
        //MAX TEMP
        String maxTemp = " " + context.getString(R.string.max_temp) + ": " +weatherMapList.get(position).getMain().getTempMax().intValue();
        holder.maxTemp.setText(maxTemp);
        //MIN TEMP
        String minTemp = " " + context.getString(R.string.min_temp) + ": " +weatherMapList.get(position).getMain().getTempMin().intValue();
        holder.minTemp.setText(minTemp);
        //pressure
        String pressure = context.getString(R.string.pressure) + ": " + weatherMapList.get(position).getMain().getPressure().intValue() + " hpa";
        holder.pressureTV.setText(pressure);
        // clouds
        String clouds = context.getString(R.string.clouds) + ": " +weatherMapList.get(position).getClouds().getAll() + " %";
        holder.cloudsAllTV.setText(clouds);
        // winds
        String winds = context.getString(R.string.winds)
                + ": " + weatherMapList.get(position).getWind().getSpeed() + " m/s\n"
                + context.getString(R.string.degree)
                + ": " + weatherMapList.get(position).getWind().getDeg();
        holder.windsTV.setText(winds);


    }

    @Override
    public int getItemCount() {
        return weatherMapList.size();
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder{
        TextView dateName, date, temp, tempDesc, humidity,
                cloud, maxTemp, minTemp, hourTV, windsTV, pressureTV, cloudsAllTV;
        ImageView weatherIcon;

        public ForecastViewHolder(@NonNull View itemView) {
            super(itemView);
            dateName = itemView.findViewById(R.id.dateNameFTV);
            hourTV = itemView.findViewById(R.id.hourFTV);
            date = itemView.findViewById(R.id.dateFTV);
            temp = itemView.findViewById(R.id.tempFTV);
            tempDesc = itemView.findViewById(R.id.temp_descriptionFTV);
            humidity = itemView.findViewById(R.id.humidityFTV);
            cloud = itemView.findViewById(R.id.cloudFTV);
            maxTemp = itemView.findViewById(R.id.temp_maxFTV);
            minTemp = itemView.findViewById(R.id.temp_minFTV);
            weatherIcon = itemView.findViewById(R.id.weatherIconFTV);
            windsTV = itemView.findViewById(R.id.windsFTV);
            pressureTV = itemView.findViewById(R.id.pressureFTV);
            cloudsAllTV = itemView.findViewById(R.id.cloudsAllFTV);
        }
    }

    public void updateCollectionData(List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherList){
        this.weatherMapList = weatherList;
        notifyDataSetChanged();
    }
}
