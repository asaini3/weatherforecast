package com.example.wetaherforeacstexercise.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import com.example.wetaherforeacstexercise.R;
import com.example.wetaherforeacstexercise.model.currentweather.CurrentWeatherData;
import com.example.wetaherforeacstexercise.network.ApiClient;
import com.example.wetaherforeacstexercise.network.ApiInterface;
import com.example.wetaherforeacstexercise.utils.Constants;
import com.example.wetaherforeacstexercise.utils.TDConverter;
import com.squareup.picasso.Picasso;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TodayTabFragment extends Fragment {


    private TextView temp, city, temp_des, date, sunrise, sunset,
        humidityTV, pressureTV, minTempTV, maxTempTV,
        cloudsTV, windTV;
    private Switch unit_switch;
    private ImageView temp_icon;
    private Context context;
    private String weatherMap_url;
    private String switch_unit_status = "metric";


    public TodayTabFragment() {
      
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_today_tab, container, false);
        temp = view.findViewById(R.id.tempTV);
        city = view.findViewById(R.id.cityTV);
        date = view.findViewById(R.id.dateTV);
        temp_des = view.findViewById(R.id.descriptionWeather);
        sunrise = view.findViewById(R.id.sunriseTV);
        sunset = view.findViewById(R.id.sunsetTV);
        humidityTV = view.findViewById(R.id.humidityTV);
        pressureTV = view.findViewById(R.id.pressureTV);
        cloudsTV = view.findViewById(R.id.cloudsTV);
        windTV = view.findViewById(R.id.windsTV);
        maxTempTV = view.findViewById(R.id.tempMaxTV);
        minTempTV = view.findViewById(R.id.tempMinTV);
        temp_icon = view.findViewById(R.id.weatherImage);
        unit_switch = view.findViewById(R.id.unit_switch);
        unit_switch.setChecked(false);
        unit_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    switch_unit_status = "imperial";
                    getWeatherByUnit(switch_unit_status);
                } else {
                    switch_unit_status = "metric";
                    getWeatherByUnit(switch_unit_status);
                }
            }
        });
        getWeatherByUnit(switch_unit_status);


        return view;
    }


    private void getWeatherByUnit( String switch_unit_status) {
        ApiInterface apiInterface = ApiClient.getClient(Constants.WEATHER_MAP_BASE_URL).create(ApiInterface.class);

        double latitude = getArguments().getDouble("lat");
        double longitude = getArguments().getDouble("lng");

        weatherMap_url = String.format("weather?lat=%f&lon=%f&units=%s&appid=%s", latitude, longitude, switch_unit_status, Constants.WEATHER_MAP_API);

        apiInterface.getCurrentWeatherData(weatherMap_url)
                .enqueue(new Callback<CurrentWeatherData>() {
                    @Override
                    public void onResponse(Call<CurrentWeatherData> call, Response<CurrentWeatherData> response) {
                        if (response.isSuccessful()){
                            CurrentWeatherData currentWeatherData = response.body();

                            // weather icon
                            String weatherIcon = currentWeatherData.getWeather().get(0).getIcon();
                            String iconUrl = Constants.WEATHER_MAP_IMAGE_BASE_URL+weatherIcon+".png";
                            Picasso.get()
                                    .load(iconUrl)
                                    .into(temp_icon);

                            // weather in celsius and Fahrenheit & winds in sec and hour
                            int Temp = currentWeatherData.getMain().getTemp().intValue();
                            String winds = getString(R.string.winds) + "\n" +currentWeatherData.getWind().getSpeed() ;
                            if (unit_switch.isChecked()){
                                String tempInF = Temp + " " + getString(R.string.unit_f);
                                temp.setText(tempInF);

                                String windsKInF = winds + " m/h";
                                windTV.setText(windsKInF);
                            } else {
                                String tempInC = Temp + " " + getString(R.string.unit_c);
                                temp.setText(tempInC);

                                String windsMInF = winds + " m/s";
                                windTV.setText(windsMInF);
                            }

                            // weather description
                            temp_des.setText(currentWeatherData.getWeather().get(0).getDescription());

                            // City name and Country CODE
                            String userCity = currentWeatherData.getName() + ", ";
                            String userCountry = currentWeatherData.getSys().getCountry();
                            String city_country = userCity + userCountry;
                            city.setText(city_country);

                            // today date
                            String dateString = getString(R.string.today) + " " + TDConverter.getDate(currentWeatherData.getDt());
                            date.setText(dateString);

                            //sunrise time
                            String sunriseString = getString(R.string.sunrise) + " " + TDConverter.getTime(currentWeatherData.getSys().getSunrise()) +", ";
                            sunrise.setText(sunriseString);

                            //sunset time
                            String sunsetString = getString(R.string.senset) + " " + TDConverter.getTime(currentWeatherData.getSys().getSunset());
                            sunset.setText(sunsetString);

                            // humidity
                            String humidity = getString(R.string.humidity) + "\n" + currentWeatherData.getMain().getHumidity() + " %";
                            humidityTV.setText(humidity);

                            //pressure
                            String pressure = getString(R.string.pressure) + "\n" +currentWeatherData.getMain().getPressure().intValue() + " hpa";
                            pressureTV.setText(pressure);

                            // clouds
                            String clouds = getString(R.string.clouds) + "\n" +currentWeatherData.getClouds().getAll() + " %";
                            cloudsTV.setText(clouds);


                            //MAX TEMP
                            String maxTemp = getString(R.string.max_temp) + "\n" +currentWeatherData.getMain().getTempMax().intValue();
                            maxTempTV.setText(maxTemp);

                            //MIN TEMP
                            String minTemp = getString(R.string.min_temp) + "\n" +currentWeatherData.getMain().getTempMin().intValue();
                            minTempTV.setText(minTemp);
                        }
                    }
                    @Override
                    public void onFailure(Call<CurrentWeatherData> call, Throwable t) {
                    }
                });

    }

}
