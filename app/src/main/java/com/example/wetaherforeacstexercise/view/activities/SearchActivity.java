package com.example.wetaherforeacstexercise.view.activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.example.wetaherforeacstexercise.R;
import com.example.wetaherforeacstexercise.adapter.SearchForecastAdapter;
import com.example.wetaherforeacstexercise.model.currentweather.CurrentWeatherData;
import com.example.wetaherforeacstexercise.model.forecastweather.ForecastWeatherData;
import com.example.wetaherforeacstexercise.model.forecastweather.List;
import com.example.wetaherforeacstexercise.network.ApiClient;
import com.example.wetaherforeacstexercise.network.ApiInterface;
import com.example.wetaherforeacstexercise.utils.Constants;
import com.example.wetaherforeacstexercise.utils.TDConverter;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText searchInput;
    private ImageView backButton;
    private ImageButton searchBtn;

    private TextView searchDetailsTV;
    private TextView temp, city, temp_des, date, sunrise, sunset,
            humidityTV, pressureTV, minTempTV, maxTempTV,
            cloudsTV, windTV, forecastTVSearch;
    private Switch unit_switch;
    private ImageView temp_icon;
    private String weather_url, forecast_weather_url;
    private String switch_unit_status = "metric";
    private String searchString;
    private SearchForecastAdapter adapter;
    private RecyclerView weatherListRV;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        
        toolbar = findViewById(R.id.search_appbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater layoutInflater = (LayoutInflater)
                this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.appbar_search, null);
        actionBar.setCustomView(view);

        searchInput = findViewById(R.id.serachInput);
        backButton = findViewById(R.id.backButton);
        searchBtn = findViewById(R.id.searchBtn);
        temp = findViewById(R.id.tempTVSearch);
        city = findViewById(R.id.cityTVSearch);
        date = findViewById(R.id.dateTVSearch);
        temp_des = findViewById(R.id.descriptionWeatherSearch);
        temp_icon = findViewById(R.id.weatherImageSearch);
        sunrise = findViewById(R.id.sunriseTVSearch);
        sunset = findViewById(R.id.sunsetTVSearch);
        humidityTV = findViewById(R.id.humidityTVSearch);
        pressureTV = findViewById(R.id.pressureTVSearch);
        cloudsTV = findViewById(R.id.cloudsTVSearch);
        windTV = findViewById(R.id.windsTVSearch);
        maxTempTV = findViewById(R.id.tempMaxTVSearch);
        minTempTV = findViewById(R.id.tempMinTVSearch);
        unit_switch = findViewById(R.id.unit_switchSearch);
        forecastTVSearch = findViewById(R.id.forecastTVSearch);
        unit_switch.setChecked(false);
        weatherListRV = findViewById(R.id.weatherListSearch);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchDetailsTV = findViewById(R.id.searchDetailsTV);
        findViewById(R.id.relLaySearch).setVisibility(View.GONE);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchString = searchInput.getText().toString();
                if (TextUtils.isEmpty(searchString)){
                    findViewById(R.id.relLaySearch).setVisibility(View.GONE);
                    Toast.makeText(SearchActivity.this, "Type a city", Toast.LENGTH_SHORT).show();
                }

                unit_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b){
                            switch_unit_status = "imperial";
                            getWeatherByCityNUnit(searchString.toLowerCase(), switch_unit_status);
                        } else {
                            switch_unit_status = "metric";
                            getWeatherByCityNUnit(searchString.toLowerCase(), switch_unit_status);
                        }

                    }
                });
                getWeatherByCityNUnit(searchString.toLowerCase(), switch_unit_status);
                findViewById(R.id.relLaySearch).setVisibility(View.GONE);
            }
        });

    } 

    ApiInterface apiInterface = ApiClient.getClient(Constants.WEATHER_MAP_BASE_URL).create(ApiInterface.class);

    private void getWeatherByCityNUnit(final String searchString, final String temp_unit) {
        findViewById(R.id.relLaySearch).setVisibility(View.GONE);

        // weather for search city
        weather_url = String.format("weather?q=%s&units=%s&appid=%s", searchString, temp_unit, Constants.WEATHER_MAP_API);
        apiInterface.getCurrentWeatherData(weather_url)
                .enqueue(new Callback<CurrentWeatherData>() {
                    @Override
                    public void onResponse(Call<CurrentWeatherData> call, Response<CurrentWeatherData> response) {
                        if (response.isSuccessful()){
                            CurrentWeatherData currentWeatherData = response.body();

                            String cityName = currentWeatherData.getName();
                            if (cityName.toLowerCase().contains(searchString)){
                                searchDetailsTV.setVisibility(View.GONE);
                                findViewById(R.id.relLaySearch).setVisibility(View.VISIBLE);

                                // weather icon
                                String weatherIcon = currentWeatherData.getWeather().get(0).getIcon();
                                String iconUrl = Constants.WEATHER_MAP_IMAGE_BASE_URL+weatherIcon+".png";
                                Picasso.get()
                                        .load(iconUrl)
                                        .into(temp_icon);

                                // weather in celsius and Fahrenheit & winds in sec and hour
                                int Temp = currentWeatherData.getMain().getTemp().intValue();
                                String winds = getString(R.string.winds) + "\n" +currentWeatherData.getWind().getSpeed() ;
                                if (unit_switch.isChecked()){
                                    String tempInF = Temp + " " + getString(R.string.unit_f);
                                    temp.setText(tempInF);

                                    String windsKInF = winds + " m/h";
                                    windTV.setText(windsKInF);
                                } else {
                                    String tempInC = Temp + " " + getString(R.string.unit_c);
                                    temp.setText(tempInC);

                                    String windsMInF = winds + " m/s";
                                    windTV.setText(windsMInF);
                                }

                                // weather description
                                temp_des.setText(currentWeatherData.getWeather().get(0).getDescription());

                                // City name and Country CODE
                                String userCountry = currentWeatherData.getSys().getCountry();
                                String city_country = cityName + ", " + userCountry;
                                city.setText(city_country);

                                //City, 5 Days / 3 Hour Forecast TV
                                String s = cityName + ", 5 Days / 3 Hour Forecast";
                                forecastTVSearch.setText(s);

                                // today date
                                String dateString = getString(R.string.today) + " " + TDConverter.getDate(currentWeatherData.getDt());
                                date.setText(dateString);

                                //sunrise time
                                String sunriseString = getString(R.string.sunrise) + " " + TDConverter.getTime(currentWeatherData.getSys().getSunrise()) +", ";
                                sunrise.setText(sunriseString);

                                //sunset time
                                String sunsetString = getString(R.string.senset) + " " + TDConverter.getTime(currentWeatherData.getSys().getSunset());
                                sunset.setText(sunsetString);

                                // humidity
                                String humidity = getString(R.string.humidity) + "\n" + currentWeatherData.getMain().getHumidity() + " %";
                                humidityTV.setText(humidity);

                                //pressure
                                String pressure = getString(R.string.pressure) + "\n" +currentWeatherData.getMain().getPressure().intValue() + " hpa";
                                pressureTV.setText(pressure);

                                // clouds
                                String clouds = getString(R.string.clouds) + "\n" +currentWeatherData.getClouds().getAll() + " %";
                                cloudsTV.setText(clouds);

                                //MAX TEMP
                                String maxTemp = getString(R.string.max_temp) + "\n" +currentWeatherData.getMain().getTempMax().intValue();
                                maxTempTV.setText(maxTemp);

                                //MIN TEMP
                                String minTemp = getString(R.string.min_temp) + "\n" +currentWeatherData.getMain().getTempMin().intValue();
                                minTempTV.setText(minTemp);
                            }

                        } else {
                            searchDetailsTV.setVisibility(View.VISIBLE);
                            searchDetailsTV.setText(Html.fromHtml("Oops!! <font color='blue'>'"+searchString+"'</font> Not Found"));
                            findViewById(R.id.relLaySearch).setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onFailure(Call<CurrentWeatherData> call, Throwable t) {
                        Log.e("onFailure", "SearchWeatherByCity: "+t.getLocalizedMessage()+"\n"+
                                t.getMessage());
                        Toast.makeText(SearchActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        //Forecast weather List for searching city
        forecast_weather_url = String.format("forecast?q=%s&units=%s&appid=%s", searchString, temp_unit, Constants.WEATHER_MAP_API);
        apiInterface.getForecastWeatherData(forecast_weather_url)
                .enqueue(new Callback<ForecastWeatherData>() {
                    @Override
                    public void onResponse(Call<ForecastWeatherData> call, Response<ForecastWeatherData> response) {
                        if (response.isSuccessful()){
                            ForecastWeatherData forecastWeatherData = response.body();
                            java.util.List<List> weatherLists = forecastWeatherData.getList();

                            adapter = new SearchForecastAdapter(weatherLists, SearchActivity.this, temp_unit);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchActivity.this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            weatherListRV.setLayoutManager(linearLayoutManager);
                            if (adapter!=null){
                                weatherListRV.setAdapter(adapter);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<ForecastWeatherData> call, Throwable t) {
                        Toast.makeText(SearchActivity.this, " Failed to load"+ t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("ForecastFailed: ", t.getMessage() + "\n"+t.getLocalizedMessage());
                    }
                });
    }

}
