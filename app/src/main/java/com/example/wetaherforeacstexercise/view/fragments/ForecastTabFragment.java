package com.example.wetaherforeacstexercise.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.example.wetaherforeacstexercise.R;
import com.example.wetaherforeacstexercise.adapter.ForecastAdapter;
import com.example.wetaherforeacstexercise.model.forecastweather.ForecastWeatherData;
import com.example.wetaherforeacstexercise.network.ApiClient;
import com.example.wetaherforeacstexercise.network.ApiInterface;
import com.example.wetaherforeacstexercise.utils.Constants;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForecastTabFragment extends Fragment {
    private RecyclerView weatherListRV;
    private TextView cityFTV;
    private ForecastAdapter adapter;
    private String forecast_weather_url;


    public ForecastTabFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forecast_tab, container, false);


        weatherListRV = view.findViewById(R.id.weatherListRV);
        cityFTV = view.findViewById(R.id.cityFTV);



        ApiInterface apiInterface = ApiClient.getClient(Constants.WEATHER_MAP_BASE_URL).create(ApiInterface.class);
        double latitude = getArguments().getDouble("lat");
        double longitude = getArguments().getDouble("lng");
        forecast_weather_url = String.format("forecast?lat=%f&lon=%f&units=metric&appid=%s", latitude, longitude, Constants.WEATHER_MAP_API);

        apiInterface.getForecastWeatherData(forecast_weather_url)
                .enqueue(new Callback<ForecastWeatherData>() {
                    @Override
                    public void onResponse(Call<ForecastWeatherData> call, Response<ForecastWeatherData> response) {
                        if (response.isSuccessful()){
                            ForecastWeatherData forecastWeatherData = response.body();

                            List<com.example.wetaherforeacstexercise.model.forecastweather.List> weatherLists = forecastWeatherData.getList();

                            String userCity = forecastWeatherData.getCity().getName()+ ", ";
                            String userCountry = forecastWeatherData.getCity().getCountry();
                            String city_country = "Weather in " + userCity + userCountry;
                            cityFTV.setText(city_country);

                            if (adapter == null){
                                adapter = new ForecastAdapter(weatherLists, getContext());
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                                weatherListRV.setLayoutManager(linearLayoutManager);
                                weatherListRV.setAdapter(adapter);
                            } else {
                                adapter.updateCollectionData(weatherLists);

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ForecastWeatherData> call, Throwable t) {
                        Toast.makeText(getContext(), " Failed to load"+ t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("ForecastFailed: ", t.getMessage() + "\n"+t.getLocalizedMessage());
                    }
                });


        return view;
    }

}
