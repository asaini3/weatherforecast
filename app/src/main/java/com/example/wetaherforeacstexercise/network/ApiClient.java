package com.example.wetaherforeacstexercise.network;

import com.example.wetaherforeacstexercise.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiClient {


    public static Retrofit getClient(String baseUrl ){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.WEATHER_MAP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
