package com.example.wetaherforeacstexercise.network;

import com.example.wetaherforeacstexercise.model.currentweather.CurrentWeatherData;
import com.example.wetaherforeacstexercise.model.forecastweather.ForecastWeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {


    @GET
    Call<CurrentWeatherData> getCurrentWeatherData(@Url String endUrl);

    @GET
    Call<ForecastWeatherData> getForecastWeatherData(@Url String forecastEndUrl);

}


