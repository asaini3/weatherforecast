package com.example.wetaherforeacstexercise.utils;

public class Constants {

    public final static String WEATHER_MAP_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public final static String WEATHER_MAP_IMAGE_BASE_URL = "https://openweathermap.org/img/w/";
    public final static String WEATHER_MAP_API = "b1e8f1fb1064a5c4b7b0de378eb18905";
    // Lat & Lag for Bangalore
    public final static double DEFAULT_LAT = 12.9716;
    public final static double DEFAULT_LNG =  77.5946;

}
